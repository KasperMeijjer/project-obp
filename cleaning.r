install.packages("missForest")
library(missForest)
recruitmentdata_training <- read.csv("F:/BA project 2017/recruitmentdata_training.csv", header = TRUE, stringsAsFactors=FALSE)
recruitmentdata_training <- read.csv("C:/Users/Mirjam/Google Drive/Vu/Project OBP/recruitmentdata_training.csv", header = TRUE, stringsAsFactors=FALSE)
setwd("C:/Users/Mirjam/Google Drive/Vu/Project OBP/")

#calculate application rate
training <- as.data.frame(recruitmentdata_training, stringsAsFactors=FALSE);
training2 <- subset(training, select=-c(X, date, vacancy_title, location2,  vac_pageviews, applications))

freqVac <- rle(sort(training$vacancy_id))
freqDF <- data.frame(number=freqVac$values, n=freqVac$lengths)

freqApplication <- aggregate( applications ~ vacancy_id, data = training, FUN = sum)

freqDF["applications"] = freqApplication[,2]
freqDF["application_rate"] = (freqDF[3] / freqDF[2]) * 7;

#switch min max
minAdj <- ifelse(training2$min_hours > training2$max_hours, training2$max_hours, training2$min_hours)
maxAdj <- ifelse(training2$min_hours < training2$max_hours, training2$max_hours, training2$min_hours)
# replace hours above 40 with 40
maxAdj[maxAdj > 40] <- 40
minAdj[minAdj > 40] <- 40

training2$min_hours_adj <- minAdj
training2$max_hours_adj <- maxAdj

#insert smaller hash values
edu_cat = replicate(1000, paste(sample(LETTERS, 5, replace=TRUE), collapse=""))
length(unique(edu_cat))

#give every unique hash value a number
training2 <- transform(training2,edu_lvl_adj=as.numeric(factor(edu_lvl)))
training2 <- transform(training2,field_adj=as.numeric(factor(field)))
training2 <- transform(training2,target_group_adj=as.numeric(factor(target_group)))
#training2 <- transform(training2,vacancy_title_adj=as.numeric(factor(vacancy_title)))
training2 <- transform(training2,department_adj=as.numeric(factor(department)))

#when you want to change to characters
training2$edu_lvl_adj <- edu_cat[training2$edu_lvl_adj]
training2$field_adj <- edu_cat[training2$field_adj]
training2$target_group_adj <- edu_cat[training2$target_group_adj]
#training2$vacancy_title_adj <- edu_cat[training2$vacancy_title_adj]
training2$department_adj <- edu_cat[training2$department_adj]

#changing back to factors
training2$edu_lvl_adj <- as.factor(training2$edu_lvl_adj)
training2$field_adj <- as.factor(training2$field_adj)
training2$target_group_adj <- as.factor(training2$target_group_adj)
#training2$vacancy_title_adj <- as.factor(training2$vacancy_title_adj)
training2$department_adj <- as.factor(training2$department_adj)

#make boolean for changed values
#only make hash values smaller in new column for visuals but not changed the data itself.
training2$edu_lvl_changed_boolean <- ifelse(is.na(training2$edu_lvl), TRUE, FALSE)
training2$field_changed_boolean <- ifelse(is.na(training2$field), TRUE, FALSE)
training2$target_group_changed_boolean <- ifelse(is.na(training2$target_group), TRUE, FALSE)
#training2$vacancy_title_changed_boolean <- ifelse(is.na(training2$vacancy_title), TRUE, FALSE)
training2$department_changed_boolean <- ifelse(is.na(training2$department), TRUE, FALSE)

#original data
training2$process_changed_boolean <- ifelse(is.na(training2$process), TRUE, FALSE)
training2$location1_changed_boolean <- ifelse(is.na(training2$process), TRUE, FALSE)
  
#data with some logical changes
training2$min_hours_changed_boolean <- ifelse((training2$min_hours == training2$min_hours_adj) & (!is.na(training2$min_hours)), FALSE, TRUE)
training2$max_hours_changed_boolean <- ifelse((training2$max_hours == training2$max_hours_adj) & (!is.na(training2$max_hours)), FALSE, TRUE)


# aggregate training set
training3 <- subset(training2, select = -c(min_hours, max_hours))
#training3$minAdj <- minAdj
#training3$maxAdj <- maxAdj
trainingset.aggregated <- subset(training3,!duplicated(training3$vacancy_id))
trainingset.aggregated["application_rate"] <- freqDF[4]


#number of location1 < 25 times in trainingset
freqLoc <- table(trainingset.aggregated$location1)
freqLocDF <- as.data.frame(freqLoc)
location.amountBelow30 = 0;
for(i in 1:length(freqLocDF$Var1)){
  if(freqLocDF[i, 2] < 30){
    location.amountBelow30 = location.amountBelow30  + freqLocDF[i, 2];
  }
}

for(i in 1 : length(trainingset.aggregated$vacancy_id)) {
  #print(trainingset.aggregated$location1[i])
  a = which(freqLocDF == trainingset.aggregated$location1[i])[1]

  if(!is.na(a) && a < 30){
    trainingset.aggregated$location1[i] = "below30"
  }
}

trainingset.aggregated2 <- as.data.frame(trainingset.aggregated)
trainingset.aggregated2$edu_lvl <- as.factor(trainingset.aggregated2$edu_lvl)
trainingset.aggregated2$field <- as.factor(trainingset.aggregated2$field)
trainingset.aggregated2$target_group <- as.factor(trainingset.aggregated2$target_group)
trainingset.aggregated2$department <- as.factor(trainingset.aggregated2$department)
trainingset.aggregated2$location1 <- as.factor(trainingset.aggregated2$location1)
trainingset.aggregated2$process <- as.factor(trainingset.aggregated2$process)
trainingset.aggregated2$min_hours_adj <- as.factor(trainingset.aggregated2$min_hours_adj)
trainingset.aggregated2$max_hours_adj <- as.factor(trainingset.aggregated2$max_hours_adj)
#add booleans
trainingset.aggregated2$edu_lvl_changed_boolean <- as.factor(trainingset.aggregated2$edu_lvl_changed_boolean)
trainingset.aggregated2$field_changed_boolean <- as.factor(trainingset.aggregated2$field_changed_boolean)
trainingset.aggregated2$target_group_changed_boolean <- as.factor(trainingset.aggregated2$target_group_changed_boolean)
trainingset.aggregated2$department_changed_boolean <- as.factor(trainingset.aggregated2$department_changed_boolean)
trainingset.aggregated2$process_changed_boolean <- as.factor(trainingset.aggregated2$process_changed_boolean)
trainingset.aggregated2$location1_changed_boolean <- as.factor(trainingset.aggregated2$location1_changed_boolean)
trainingset.aggregated2$min_hours_changed_boolean <- as.factor(trainingset.aggregated2$min_hours_changed_boolean)
trainingset.aggregated2$max_hours_changed_boolean <- as.factor(trainingset.aggregated2$max_hours_changed_boolean)
#add hashed
trainingset.aggregated2$edu_lvl_adj <- as.factor(trainingset.aggregated2$edu_lvl_adj)
trainingset.aggregated2$field_adj <- as.factor(trainingset.aggregated2$field_adj)
trainingset.aggregated2$target_group_adj <- as.factor(trainingset.aggregated2$target_group_adj)
trainingset.aggregated2$department_adj <- as.factor(trainingset.aggregated2$department_adj)


#Solve missing values with missForest
#trainingset.aggregated2 <- subset(trainingset.aggregated2, select = -c(vacancy_id))
tempData <- missForest(trainingset.aggregated2)

training.filled <- tempData$ximp

# write with "," and sep ";"
write.csv2(training.filled, file = "cleaned_recruitmentdata_training.csv", row.names=FALSE)

#prediction

