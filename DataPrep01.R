# edu lvl orderd data. youu dont know the order
trainRec = recruitmentdata_training
unique(trainRec$edu_lvl)
summary(trainRec$edu_lvl)

edu_cat = replicate(1000, paste(sample(LETTERS, 4, replace=TRUE), collapse=""))
length(unique(edu_cat))

#give every unique hash value a number
trainRec <- transform(trainRec,edu_lvl_adj=as.numeric(factor(edu_lvl)))
trainRec <- transform(trainRec,field_adj=as.numeric(factor(field)))
trainRec <- transform(trainRec,target_group_adj=as.numeric(factor(target_group)))
trainRec <- transform(trainRec,vacancy_title_adj=as.numeric(factor(vacancy_title)))
trainRec <- transform(trainRec,department_adj=as.numeric(factor(department)))

summary(trainRec$field_adj)
summary(trainRec$target_group_adj)
summary(trainRec$vacancy_title_adj)
summary(trainRec$department_adj)

#when you want to change to characters
trainRec$edu_lvl_adj <- edu_cat[trainRec$edu_lvl_adj]
trainRec$field_adj <- edu_cat[trainRec$field_adj]
trainRec$target_group_adj <- edu_cat[trainRec$target_group_adj]
trainRec$vacancy_title_adj <- edu_cat[trainRec$vacancy_title_adj]
trainRec$department_adj <- edu_cat[trainRec$department_adj]

#changing back to factors
trainRec$edu_lvl_adj <- as.factor(trainRec$edu_lvl_adj)
trainRec$field_adj <- as.factor(trainRec$field_adj)
trainRec$target_group <- as.factor(trainRec$target_group)
trainRec$vacancy_title_adj <- as.factor(trainRec$vacancy_title_adj)
trainRec$department_adj <- as.factor(trainRec$department_adj)


summary(trainRec$vacancy_title_adj)

trainRec$min_hours_adj = trainRec$min_hours
trainRec$max_hours_adj = trainRec$max_hours

#
trainRec$min_hours_adj <- ifelse(trainRec$min_hours>trainRec$max_hours,trainRec$max_hours,trainRec$min_hours)
trainRec$max_hours_adj <- ifelse(trainRec$max_hours<trainRec$min_hours,trainRec$min_hours,trainRec$max_hours)


summary(trainRec$location1)

